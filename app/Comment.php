<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function User()
    {
        return $this->belongsTo('App\User');
    }

    public function Commenttype()
    {
        return $this->belongsTo('App\Commenttype');
    }

    public function Status()
    {
        return $this->belongsTo('App\Status');
    }

    public function Defect()
    {
        return $this->belongsTo('App\Defect');
    }
}
