<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Defect extends Model
{

       /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'information', 'room_id', 'defectcategory_id', 'user_id', 'status_id',
    ];


    public function User()
    {
        return $this->belongsTo('App\User');
    }

    public function Room()
    {
        return $this->belongsTo('App\Room');
    }

    public function Status()
    {
        return $this->belongsTo('App\Status');
    }

    public function Comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function DefectCategory()
    {
        return $this->belongsTo('App\Defect_Category', 'defectcategory_id');
    }
/*
    public function getLaststatusAttribute()
    {
        return $this->Status()->orderBy('created_at')->first();
    }
    */
}
