<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class defect_category extends Model
{
    public function defects()
    {
        return $this->hasMany('App\Defect', 'defectcategory_id');
    }
}
