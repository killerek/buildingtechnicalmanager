<?php

namespace App\Http\Controllers;

use App\commenttype;
use Illuminate\Http\Request;

class CommenttypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\commenttype  $commenttype
     * @return \Illuminate\Http\Response
     */
    public function show(commenttype $commenttype)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\commenttype  $commenttype
     * @return \Illuminate\Http\Response
     */
    public function edit(commenttype $commenttype)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\commenttype  $commenttype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, commenttype $commenttype)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\commenttype  $commenttype
     * @return \Illuminate\Http\Response
     */
    public function destroy(commenttype $commenttype)
    {
        //
    }
}
