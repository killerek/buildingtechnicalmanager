<?php

namespace App\Http\Controllers;

use App\defect_category;
use Illuminate\Http\Request;

class DefectCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = \App\defect_category::all();

        return view('defectcategories.index', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\defect_category  $defect_category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $defects = \App\Defect::where('defectcategory_id', $id)->orderBy('updated_at', 'desc')->paginate(15);

        return view('defect.index', ['defects' => $defects]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\defect_category  $defect_category
     * @return \Illuminate\Http\Response
     */
    public function edit(defect_category $defect_category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\defect_category  $defect_category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, defect_category $defect_category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\defect_category  $defect_category
     * @return \Illuminate\Http\Response
     */
    public function destroy(defect_category $defect_category)
    {
        //
    }
}
