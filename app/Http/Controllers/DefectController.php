<?php

namespace App\Http\Controllers;

use App\Defect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DefectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $defects = \App\Defect::orderBy('updated_at', 'desc')->paginate(15);


        return view('defect.index', ['defects' => $defects]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rooms = \App\Room::all();
        $categories = \App\Defect_Category::all();
        return view('defect.create', ['rooms' => $rooms, 'categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::user()->id;

    
 
        \App\Defect::create([
          'room_id' => $request->input('rooms'),
          'defectcategory_id'=> $request->input('category'),
          'information'=> $request->input('information'),
          'status_id'=> '1',
          'user_id' => $user_id
        ]);
 
        return redirect('/defects')->with('success', 'Dodano nowe zadanie.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Defect  $defect
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $defect = \App\Defect::find($id);

        return view('defect.show', ['defect' => $defect]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Defect  $defect
     * @return \Illuminate\Http\Response
     */
    public function edit(Defect $defect)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Defect  $defect
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Defect $defect)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Defect  $defect
     * @return \Illuminate\Http\Response
     */
    public function destroy(Defect $defect)
    {
        //
    }
}
