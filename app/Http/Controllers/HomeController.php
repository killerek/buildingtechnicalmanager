<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $defects = \App\Defect::orderBy('updated_at', 'desc')->take(3)->get();
        $comments = \App\Comment::where('commenttype_id', '1')->orderBy('created_at', 'desc')->take(3)->get();
        $status_changes = \App\Comment::where('commenttype_id', '2')->orderBy('created_at', 'desc')->take(3)->get();

        return view('home', ['defects' => $defects, 'comments' => $comments, 'status_changes' => $status_changes]);
    }
}
