<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSefectStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('defect_status', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('defect_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->foreign('defect_id')->references('id')->on('defects')->onDelete('cascade');
            $table->foreign('status_id')->references('id')->on('statuses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('defect_status');
    }
}
