<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->text('information');
            $table->integer('defect_id')->unsigned();
            $table->foreign('defect_id')->references('id')->on('defects')->onDelete('cascade');
            $table->integer('commenttype_id')->unsigned();
            $table->foreign('commenttype_id')->references('id')->on('commenttypes')->onDelete('cascade');
            $table->integer('status_id')->nullable()->unsigned();
            $table->foreign('status_id')->references('id')->on('statuses')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
