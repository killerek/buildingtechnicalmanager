<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class Comment extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pl_PL');
        
        for($i=1;$i<=1000;$i++)
        {
          DB::table('comments')->insert([

            'user_id' => $faker->numberBetween(1,10),
            'defect_id' => $faker->numberBetween(1,1000),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'information' => $faker->sentence(50, true),
            'commenttype_id' => $faker->numberBetween(1,2),
            'status_id' => $faker->numberBetween(1,4),
  
          ]);
        }
    }
}
