<?php

use Illuminate\Database\Seeder;

class Commenttype extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('commenttypes')->insert([

            'name' => "Komentarz",
            'color' => "text-primary"

          ]);

          DB::table('commenttypes')->insert([

            'name' => "Zmiana Statusu",
            'color' => "text-danger"

          ]);
    }
}
