<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(Position::class); 
         $this->call(User::class);
         $this->call(Rooms::class);
         $this->call(DefectCategory::class);
         $this->call(Defects::class);
         $this->call(Status::class);
         $this->call(Commenttype::class);
         $this->call(Comment::class);
        
        // $this->call(defect_status::class);
    }
}
