<?php

use Illuminate\Database\Seeder;

class DefectCategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('defect_categories')->insert([

            'name' => "Stolarka",
            'color' => "primary",
            'icon' => "mdi-screwdriver"

          ]);

          DB::table('defect_categories')->insert([

            'name' => "Hydraulika",
            'color' => "danger",
            'icon' => "mdi-water-pump"

          ]);

          DB::table('defect_categories')->insert([

            'name' => "Mechanika",
            'color' => "cyan",
            'icon' => "mdi-wrench"

          ]);

          DB::table('defect_categories')->insert([

            'name' => "Elektryka",
            'color' => "danger",
            'icon' => "mdi-lightbulb-on"

          ]);

          DB::table('defect_categories')->insert([

            'name' => "Automatyka",
            'color' => "cyan",
            'icon' => "mdi-recycle"

          ]);

          DB::table('defect_categories')->insert([

            'name' => "Wentylacja",
            'color' => "warning",
            'icon' => "mdi-fan"

          ]);

          DB::table('defect_categories')->insert([

            'name' => "P-poż",
            'color' => "danger",
            'icon' => "mdi-fire"

          ]);

          DB::table('defect_categories')->insert([

            'name' => "Informatyka",
            'color' => "success",
            'icon' => "mdi-floppy"

          ]);

          DB::table('defect_categories')->insert([

            'name' => "Serwis",
            'color' => "secondary",
            'icon' => "mdi-oil"

          ]);

          DB::table('defect_categories')->insert([

            'name' => "Inne",
            'color' => "secondary",
            'icon' => "mdi-comment-question-outline"

          ]);
    }
}
