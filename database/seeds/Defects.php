<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;



class Defects extends Seeder


{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pl_PL');
        
        for($i=1;$i<=1000;$i++)
        {
          DB::table('defects')->insert([

            'user_id' => $faker->numberBetween(1,10),
            'room_id' => $faker->numberBetween(1,97),
            'status_id' => $faker->numberBetween(1,4),
            'defectcategory_id' => $faker->numberBetween(1,10),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'information' => $faker->sentence(50, true),
  
          ]);
        }
    }
}
