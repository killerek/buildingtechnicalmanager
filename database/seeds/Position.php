<?php

use Illuminate\Database\Seeder;

class Position extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->insert([

            'name' => "Elektryk",

          ]);

          DB::table('positions')->insert([

            'name' => "Automatyk",

          ]);

          DB::table('positions')->insert([

            'name' => "Konserwator",

          ]);

          DB::table('positions')->insert([

            'name' => "Serwisant",

          ]);

          DB::table('positions')->insert([

            'name' => "Informatyk",

          ]);


    }
}
