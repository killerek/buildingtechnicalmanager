<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class Rooms extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pl_PL');

        for($i=100;$i<=130;$i++)
        {
          DB::table('rooms')->insert([

            'number' => $i,
            'name' => 'Pokój Hotelowy - Standard',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            
  
          ]);
        }

        for($i=200;$i<=230;$i++)
        {
          DB::table('rooms')->insert([

            'number' => $i,
            'name' => 'Pokój Hotelowy - Standard',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            
  
          ]);
        }

        for($i=300;$i<=330;$i++)
        {
          DB::table('rooms')->insert([

            'number' => $i,
            'name' => 'Pokój Hotelowy - Exclusive',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            
  
          ]);
        }

        for($i=400;$i<=402;$i++)
        {
          DB::table('rooms')->insert([

            'number' => $i,
            'name' => 'Pokój Hotelowy - VIP',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            
  
          ]);
        }

        DB::table('rooms')->insert([

          'number' => 410,
          'name' => 'Pokój Hotelowy - Prezydencki',
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
          

        ]);
    }
}
