<?php

use Illuminate\Database\Seeder;

class Status extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

          DB::table('statuses')->insert([

            'name' => "Nowy",
            'color' => "text-info"

          ]);

          DB::table('statuses')->insert([

            'name' => "Przyjęty do realizacji",
            'color' => "text-warning"

          ]);

          DB::table('statuses')->insert([

            'name' => "Zrealizowany",
            'color' => "text-success"

          ]);

          DB::table('statuses')->insert([

            'name' => "Uwagi",
            'color' => "text-danger"

          ]);
        
    }
}
