<?php

use Illuminate\Database\Seeder;

class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pl_PL');

        for($i=1;$i<=10;$i++)
        {
          DB::table('users')->insert([
            'name' => $faker->firstName,
            'email' => $faker->email,
            'password' => bcrypt('password'),
            'position_id' => $faker->numberBetween(1,5)

          ]);
        }

        DB::table('users')->insert([
            'name' => "Daniel",
            'email' => "danielwezut@gmail.com",
            'password' => bcrypt('123456'),
            'position_id' => $faker->numberBetween(1,5)

          ]);

    }
}
