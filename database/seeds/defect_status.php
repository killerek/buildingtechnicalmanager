<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class defect_status extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pl_PL');
        
        for($i=1;$i<=30;$i++)
        {
          DB::table('defect_status')->insert([

            'defect_id' => $i,
            'status_id' => $faker->numberBetween(1,4),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),

          ]);
        }
    }
}
