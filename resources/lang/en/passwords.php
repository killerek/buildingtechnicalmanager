<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Hasło zostało zresetowane!',
    'sent' => 'Wysłaliśmy e-mail z linkiem resetującym hasło!',
    'token' => 'Token jest nie ważny.',
    'user' => "Nie możemy znaleźć użytkownika z podanym e-mailem.",

];
