@extends('layouts.app')

@section('content')


<div class="row justify-content-center">
    <div class="col-6">

                    <div class="card">
                            <form method="post" action="{{ route('defects.store') }}">
                                    @csrf
                            <div class="card-body">
                                <h4 class="card-title">Dodaj usterkę</h4>
                                <div class="form-group row">
                                    <label for="rooms" class="col-sm-3 text-right control-label col-form-label">Wybierz pokój: </label>
                                    <div class="col-sm-9">
                                    <select name = "rooms" name = "rooms" class="form-control">
                                    @foreach($rooms as $room)
                                            <option value = "{{$room->id}}">{{$room->number}} - {{$room->name}}</option>
                                    @endforeach
                                    </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                        <label for="category" class="col-sm-3 text-right control-label col-form-label">Wybierz kategorie: </label>
                                        <div class="col-sm-9">
                                        <select id = "category" name = "category" class="form-control">
                                        @foreach($categories as $category)
                                                <option value = "{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                        </select>
                                        </div>
                                </div>

                                <div class="form-group row">
                                        <label for="information" class="col-sm-3 text-right control-label col-form-label">Opisz zadanie: </label>
                                        <div class="col-sm-9">
                                        <textarea class="form-control" name="information" rows="3"></textarea>
                                        </div>
                                </div>
                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Dodaj</button>
                                </div>
                            </div>
                        </form>
                    </div>




</div>

@endsection