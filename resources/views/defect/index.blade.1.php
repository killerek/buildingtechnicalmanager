@extends('layouts.app')

@section('content')

<div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title m-b-0">Lista Usterek</h5>
                </div>
                <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Numer pokoju</th>
                          <th scope="col">Informacja</th>
                          <th scope="col">Dodał</th>
                          <th scope="col">Data dodania</th>
                        </tr>
                      </thead>
                      <tbody>
                    @foreach ($defects as $defect)
                        <tr>
                          <th scope="row">{{ $defect->id }}</th>
                          <td>{{ $defect->room->number }}</td>
                          <td>{{ $defect->information }}</td>
                          <td>{{ $defect->user->name }}</td>
                          <td>{{ $defect->created_at }}</td>
                        </tr>
                        @endforeach
                      </tbody>
                </table>
            </div>
        </div>
</div>

@endsection