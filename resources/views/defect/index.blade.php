@extends('layouts.app')

@section('content')

@foreach ($defects as $defect)

<div class="row justify-content-center">
  <div class="col-12">
      <div class="card">
          <div class="card-body">
              <div class="d-flex flex-row comment-row m-t-0">
              <div class="p-2"><h1><i class="text-{{ $defect->defectcategory->color }} mdi {{ $defect->defectcategory->icon }}"></i></h1></div>
                <div class="comment-text w-100">
                        <div class="m-b-15 font-medium"><h6>#{{ $defect->id }} <span class="{{ $defect->status->color }}">{{ $defect->status->name }}</span></h6></div>
                <span class="m-b-5 d-block">{{ $defect->information }} 
                </span>
                <div class="m-b-15">
                <span class="d-inline m-r-10"><p class="font-weight-bold d-inline">Kategoria: </p><span class="text-{{ $defect->defectcategory->color }}">{{ $defect->defectcategory->name }}</span></span>
                <span class="d-inline m-r-10"><p class="font-weight-bold d-inline">Budynek: </p>A</span>
                <span class="d-inline m-r-10"><p class="font-weight-bold d-inline">Pokój: </p>{{ $defect->room->number }}</span>
                <span class="d-inline m-r-10"><p class="font-weight-bold d-inline">Dodał: </p>{{ $defect->user->name }}</span>
                </div>
                <div class="comment-footer">
                    <span class="text-muted float-right">Data aktualizacji zgłoszenia: {{ $defect->updated_at }}</span> 
                    <a href="{{ URL::to('defects/' . $defect->id) }}" class="btn btn-cyan btn-sm">Otwórz</a>
                    <a href="{{ URL::to('defects/' . $defect->id) }}" class="btn btn-success btn-sm">Zmień status</a>
                    <a href="{{ URL::to('defects/' . $defect->id) }}" class="btn btn-secondary btn-sm">Skomentuj</a>
                    <a href="{{ URL::to('defects/' . $defect->id) }}" class="btn btn-danger btn-sm">Archiwizuj</a>
                    <a href="{{ URL::to('defects/' . $defect->id) }}" class="btn btn-danger btn-sm">Usuń</a>
                </div>
                <span class="float-right badge badge-danger">Wiadomości: {{ $defect->comments()->count() }}</span>
            </div>
              
          </div>
      </div>
  </div>
</div>
</div>
@endforeach

<div class="mx-auto">
    {{ $defects->links() }}
  </div>
@endsection