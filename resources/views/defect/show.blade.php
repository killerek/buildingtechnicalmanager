@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                    <div class="card-body">
                            <div class="d-flex flex-row comment-row m-t-0">
                                    <div class="p-2"><h1><i class="text-{{ $defect->defectcategory->color }} mdi {{ $defect->defectcategory->icon }}"></i></h1></div>
                              <div class="comment-text w-100">
                              <h6 class="font-medium m-b-10">#{{ $defect->id }} <span class="{{ $defect->status->color }}">{{ $defect->status->name }}</span></h6>
                              <span class="d-block m-b-15">{{ $defect->information }} 
                              </span>
                              <div class="m-b-15">
                                    <span class="d-inline m-r-10"><p class="font-weight-bold d-inline">Kategoria: </p><span class="text-{{ $defect->defectcategory->color }}">{{ $defect->defectcategory->name }}</span></span>
                              <span class="d-block m-t-10"><p class="font-weight-bold d-inline">Budynek: </p>A</span>
                              <span class="d-block m-t-10"><p class="font-weight-bold d-inline">Pokój: </p>{{ $defect->room->number }}</span>
                              <span class="d-block m-t-10"><p class="font-weight-bold d-inline">Dodał: </p>{{ $defect->user->name }}</span>
                              </div>
                              <div class="comment-footer">
                                  <span class="text-muted float-right">Data utworzenia zgłoszenia: {{ $defect->created_at }}</span> 
                                  <a href="{{ URL::to('defects/' . $defect->id) }}" class="btn btn-cyan btn-sm">Otwórz</a>
                                  <a href="{{ URL::to('defects/' . $defect->id) }}" class="btn btn-success btn-sm">Zmień status</a>
                                  <a href="{{ URL::to('defects/' . $defect->id) }}" class="btn btn-secondary btn-sm">Skomentuj</a>
                                  <a href="{{ URL::to('defects/' . $defect->id) }}" class="btn btn-danger btn-sm">Archiwizuj</a>
                                  <a href="{{ URL::to('defects/' . $defect->id) }}" class="btn btn-danger btn-sm">Usuń</a>
                              </div>
              </div>
                
            </div>
        </div>
    </div>
  </div>

  <div class="card">
        <div class="card-body">
            <h4 class="card-title">Komentarze</h4>
        </div>
        <div class="comment-widgets scrollable ps-container ps-theme-default" data-ps-id="3f9fdb42-e6c4-a5a0-6041-6a84ec9af5a1">
            <!-- Comment Row -->

            @foreach ($defect->comments as $comment)

            <div class="comment-row m-t-0">
                <div class="p-2"><img src="../../assets/images/users/1.jpg" alt="user" width="50" class="rounded-circle"></div>
                <div class="comment-text w-100">
                <div class="m-b-15 font-medium"><h6><span class='{{ $comment->commenttype->color }}'>{{ $comment->commenttype->name }}</span></h6> </div>
                    
                    <h6 class="font-medium">{{ $comment->user->name }}</h6>
                    
                    @if ($comment->commenttype->id == 1)
                    <span class="m-b-15 d-block">{{ $comment->information }} </span>
                    @else
                    <span class="m-b-15 d-block">Zmienił status na <span class= '{{ $comment->status->color}}'>{{ $comment->status->name}} </span></span>
                    <span class="m-b-15 d-block">{{ $comment->information }} </span>

                    @endif
                    <div class="comment-footer">
                        <span class="text-muted float-right">{{ $comment->created_at }}</span> 
                    </div>
                </div>
            </div>

            @endforeach

        </div>
    </div>

@endsection