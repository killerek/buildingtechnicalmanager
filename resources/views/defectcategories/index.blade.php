@extends('layouts.app')

@section('content')

<div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title m-b-0">Lista kategorii usterek</h5>
                </div>
                <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Ikona</th>
                          <th scope="col">Nazwa</th>
                          <th scope="col">Liczba usterek</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                    @foreach ($categories as $category)
                        <tr>
                          <th scope="row">{{ $category->id }}</th>
                          <td><i class="text-{{ $category->color }} mdi {{ $category->icon }}"></i></td>
                          <td>{{ $category->name }}</td>
                          <td>{{ $category->defects->count() }}</td>
                          <td><a href="{{ URL::to('defectcategories/' . $category->id) }}" class="btn btn-cyan btn-sm">Zobacz usterki</a></td>
                        </tr>
                        @endforeach
                      </tbody>
                </table>
            </div>
        </div>
</div>

@endsection