@extends('layouts.app')

@section('content')

<div class="row">
        <!-- Column -->
        <div class="col-md-6 col-lg-2 col-xlg-3">
            <div class="card card-hover">
                <div class="box bg-cyan text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-view-dashboard"></i></h1>
                    <h6 class="text-white">Strona Główna</h6>
                </div>
            </div>
        </div>
        <!-- Column -->
        <div class="col-md-6 col-lg-4 col-xlg-3">
            <div class="card card-hover">
                <div class="box bg-success text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-wrench"></i></h1>
                    <h6 class="text-white">Przeglądaj Usterki</h6>
                </div>
            </div>
        </div>
         <!-- Column -->
        <div class="col-md-6 col-lg-2 col-xlg-3">
            <div class="card card-hover">
                <div class="box bg-warning text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-chart-areaspline"></i></h1>
                    <h6 class="text-white">Statystyki</h6>
                </div>
            </div>
        </div>
        <!-- Column -->
        <div class="col-md-6 col-lg-2 col-xlg-3">
            <div class="card card-hover">
                <div class="box bg-danger text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-home"></i></h1>
                    <h6 class="text-white">Pomieszczenia</h6>
                </div>
            </div>
        </div>
        <!-- Column -->
        <div class="col-md-6 col-lg-2 col-xlg-3">
            <div class="card card-hover">
                <div class="box bg-info text-center">
                    <h1 class="font-light text-white"><i class="mdi mdi-home-modern"></i></h1>
                    <h6 class="text-white">Budynki</h6>
                </div>
            </div>
        </div>
        <!-- Column -->
        
    </div>
    <div class="row">
        <div class="col-md-6">
                <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Ostatnio dodane usterki</h4>
                        </div>
                        <div class="comment-widgets scrollable ps-container ps-theme-default" data-ps-id="3f9fdb42-e6c4-a5a0-6041-6a84ec9af5a1">
                            <!-- Comment Row -->
                
                            @foreach ($defects as $defect)
                
                            <div class="comment-row m-t-0">
                                
                                <div class="comment-text w-100">
                               
                                    <div class="m-b-15 font-medium"><h6>#{{ $defect->id }} <span class="{{ $defect->status->color }}">{{ $defect->status->name }}</span></h6></div>
                                    <span class="m-b-10 d-block">{{ $defect->information }} </span>
                                            <div class="m-b-15">
                                                    <span class="d-inline m-r-10 font-light"><p class="font-medium d-inline">Kategoria: </p><span class="text-{{ $defect->defectcategory->color }}">{{ $defect->defectcategory->name }} <i class=" mdi {{ $defect->defectcategory->icon }}"></i></span></span>
                                                    <span class="d-inline m-r-10 font-light"><p class="font-medium d-inline">Budynek: </p>A</span>
                                                    <span class="d-inline m-r-10 font-light"><p class="font-medium d-inline">Pokój: </p>{{ $defect->room->number }}</span>
                                                    <span class="d-inline m-r-10 font-light"><p class="font-medium d-inline">Dodał: </p>{{ $defect->user->name }}</span>
                                                    </div>
                                                    <div class="comment-footer">
                                                        
                                                        <span class="text-muted float-right">Data aktualizacji zgłoszenia: {{ $defect->updated_at }}</span> 
                                                        <a href="{{ URL::to('defects/' . $defect->id) }}" class="btn btn-cyan btn-sm">Zobacz</a>
                                                        
                                    </div>
                                    <span class="float-right badge badge-danger">Wiadomości: {{ $defect->comments()->count() }}</span>
                                </div>
                            </div>
                
                            @endforeach
                
                        </div>
                    </div>

                    <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Ostatnio zmienione statusy</h4>
                            </div>
                            <div class="comment-widgets scrollable ps-container ps-theme-default" data-ps-id="3f9fdb42-e6c4-a5a0-6041-6a84ec9af5a1">
                                <!-- Comment Row -->
    
                                @foreach ($status_changes as $status_change)
    
                                <div class="comment-row m-t-0">
                                    <div class="p-2"><img src="../../assets/images/users/1.jpg" alt="user" width="50" class="rounded-circle"></div>
                                    <div class="comment-text w-100">
                                    <div class="m-b-15 font-medium"><h6><span class='{{ $status_change->commenttype->color }}'>{{ $status_change->commenttype->name }}</span><span class="text-muted float-right">Usterka #{{ $status_change->defect->id }}</span></h6> </div>
                                        
                                        <h6 class="font-medium">{{ $status_change->user->name }}</h6>
                                        
                                        <span class="m-b-15 d-block">Zmienił status na <span class= '{{ $status_change->status->color}}'>{{ $status_change->status->name}} </span></span>
                                         <span class="m-b-15 d-block">{{ $status_change->information }} </span>
    
                                        <div class="comment-footer">
                                            <span class="text-muted float-right">{{ $status_change->created_at }}</span> 
                                            <a href="{{ URL::to('defects/' . $status_change->defect->id) }}" class="btn btn-danger btn-sm">Zobacz</a>
                                        </div>
                                    </div>
                                </div>
                    
                                @endforeach
                    
                            </div>
                        </div>
                        
        </div>
        <div class="col-md-6">
                <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Ostatnio dodane komentarze</h4>
                        </div>
                        <div class="comment-widgets scrollable ps-container ps-theme-default" data-ps-id="3f9fdb42-e6c4-a5a0-6041-6a84ec9af5a1">
                            <!-- Comment Row -->

                            @foreach ($comments as $comment)

                            <div class="comment-row m-t-0">
                                <div class="p-2"><img src="../../assets/images/users/1.jpg" alt="user" width="50" class="rounded-circle"></div>
                                <div class="comment-text w-100">
                                <div class="m-b-15 font-medium"><h6><span class='{{ $comment->commenttype->color }}'>{{ $comment->commenttype->name }}</span><span class="text-muted float-right">Usterka #{{ $comment->defect->id }}</span></h6> </div>
                                    
                                    <h6 class="font-medium">{{ $comment->user->name }}</h6>
                                    
                                    <span class="m-b-15 d-block">{{ $comment->information }} </span>

                                    <div class="comment-footer">
                                        <span class="text-muted float-right">{{ $comment->created_at }}</span> 
                                        <a href="{{ URL::to('defects/' . $comment->defect->id) }}" class="btn btn-primary btn-sm">Zobacz</a>
                                    </div>
                                </div>
                            </div>
                
                            @endforeach
                
                        </div>
                    </div>
                           

                    </div>
        </div>
    </div>
    
    


@endsection
