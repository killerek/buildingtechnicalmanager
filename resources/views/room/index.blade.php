@extends('layouts.app')

@section('content')

<div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title m-b-0">Lista pomieszczeń</h5>
                </div>
                <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Numer Pokoju</th>
                          <th scope="col">Opis</th>
                          <th scope="col">Ilość usterek</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                    @foreach ($rooms as $room)
                        <tr>
                          <th scope="row">{{ $room->id }}</th>
                          <td>{{ $room->number }}</td>
                          <td>{{ $room->name }}</td>
                          <td>{{ $room->defect->count() }}</td>
                          <td><a href="{{ URL::to('rooms/' . $room->id) }}" class="btn btn-cyan btn-sm">Otwórz</a></td>
                        </tr>
                        @endforeach
                      </tbody>
                </table>
            </div>
        </div>
</div>

@endsection