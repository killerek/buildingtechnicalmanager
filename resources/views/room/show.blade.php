@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                    <div class="card-body">
                            <div class="d-flex flex-row comment-row m-t-0">
                            <div class="p-2"><h1><i class="mdi mdi-home"></i></h1></div>
                              <div class="comment-text w-100">
                              <span class="font-medium"><h6>Numer: <span class="text-info">{{ $room->number }}</h6></span></span>
                              <span class="d-block"> 
                              </span>
                              <div class="m-b-15">
                              <span class="d-block m-t-10"><p class="font-weight-bold d-inline">Budynek: </p>A</span>
                              <span class="d-block m-t-10"><p class="font-weight-bold d-inline">Pomieszczenie: </p>{{ $room->name }}</span>
                              </div>
                              <div class="comment-footer">

                                 
              </div>
                
            </div>
        </div>
    </div>
  </div>
  <div class="card">
    <div class="card-body">
        <h4 class="card-title">Usterki</h4>
    </div>
    <div class="comment-widgets scrollable ps-container ps-theme-default" data-ps-id="3f9fdb42-e6c4-a5a0-6041-6a84ec9af5a1">
        <!-- Comment Row -->

        @foreach ($room->defect as $defect)

        <div class="comment-row m-t-0">
                <div class="p-2"><h1><i class="text-{{ $defect->defectcategory->color }} mdi {{ $defect->defectcategory->icon }}"></i></h1></div>
            <div class="comment-text w-100">
           
                <div class="m-b-15 font-medium"><h6>#{{ $defect->id }} <span class="{{ $defect->status->color }}">{{ $defect->status->name }}</span></h6></div>
                <span class="m-b-10 d-block">{{ $defect->information }} </span>
                        <div class="m-b-15">

                                <span class="d-inline m-r-10 font-light"><p class="font-medium d-inline">Dodał: </p>{{ $defect->user->name }}</span>
                                </div>
                                <div class="comment-footer">
                                    <span class="text-muted float-right">Data aktualizacji zgłoszenia: {{ $defect->updated_at }}</span> 
                                    <a href="{{ URL::to('defects/' . $defect->id) }}" class="btn btn-cyan btn-sm">Zobacz</a>
                </div>
                <span class="float-right badge badge-danger">Wiadomości: {{ $defect->comments()->count() }}</span>
            </div>
        </div>

        @endforeach

    </div>
</div>

@endsection