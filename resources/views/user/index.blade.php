@extends('layouts.app')

@section('content')

<div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title m-b-0">Lista użytkowników</h5>
                </div>
                <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Imię</th>
                          <th scope="col">E-mail</th>
                          <th scope="col">Uprawnienia</th>
                          <th scope="col">Grupy</th>
                          <th scope="col">Ilość dodanych usterek</th>
                          <th scope="col">Data utworzenia</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                    @foreach ($users as $user)
                        <tr>
                          <th scope="row">{{ $user->id }}</th>
                          <td>{{ $user->name }}</td>
                          <td>{{ $user->email }}</td>
                          <td>Użytkownik</td>
                          <td>{{ $user->position->name }}</td>
                          <td>{{ $user->defects->count() }}</td>
                          <td>{{ $user->created_at }}</td>
                          <td><a href="{{ URL::to('user/' . $user->id) }}" class="btn btn-cyan btn-sm">Otwórz</a></td>
                        </tr>
                        @endforeach
                      </tbody>
                </table>
            </div>
        </div>
</div>

@endsection