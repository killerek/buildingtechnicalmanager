@extends('layouts.app')

@section('content')

<div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title m-b-0">Lista użytkowników</h5>
                </div>
                <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Nazwa</th>
                          <th scope="col">Ilość użytkowników</th>
                          <th scope="col">Przypisane kategorie</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                    @foreach ($positions as $position)
                        <tr>
                          <th scope="row">{{ $position->id }}</th>
                          <td>{{ $position->name }}</td>
                        <td>{{ $position->users->count() }}</td>
                          <td>b/d</td>
                        </tr>
                        @endforeach
                      </tbody>
                </table>
            </div>
        </div>
</div>

@endsection